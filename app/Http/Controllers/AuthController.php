<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function form() {
        return view('form');
    }

    public function send(Request $request) {
        $nama_depan = $request["first_name"];
        $nama_belakang = $request["last_name"];
        $nama_lengkap = $nama_depan . ' ' . $nama_belakang;
        return view('welcome',compact('nama_depan','nama_belakang','nama_lengkap'));
    }

}
