<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Cast;

use Auth;

class Cast_Controller extends Controller
{

    /**
     * Instantiate a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth')->only(['create','store','edit','update','delete']);

        // $this->middleware('log')->only('index');

        // $this->middleware('subscribed')->except('store');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $cast = Cast::all();
        return view('cast-orm.index', compact('cast'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    	return view('cast-orm.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    	$this->validate($request,[
    		'nama' => 'required',
    		'umur' => 'required',
            'bio' => 'required',

    	]);
 
        Cast::create([
    		'nama' => $request->nama,
            'umur' => $request->umur,
    		'bio' => $request->bio,
            'user_id' => Auth::user()->id

    	]);
 
    	return redirect('/cast');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function show($id)
    {
        $cast = Cast::find($id);
        return view('cast-orm.show', compact('cast'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function edit($id)
    {
        $cast = Cast::find($id);
        return view('cast-orm.edit', compact('cast'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id, Request $request)
    {
        $request->validate([
            'nama' => 'required|unique:cast',
            'umur' => 'required',
            'bio' => 'required'

        ]);

        $cast = Cast::find($id);
        $cast->nama = $request->nama;
        $cast->umur = $request->umur;
        $cast->bio = $request->bio;

        $cast->update();
        return redirect('/cast');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function destroy($id)
    {
        $cast = Cast::find($id);
        $cast->delete();
        return redirect('/cast');
    }
}
