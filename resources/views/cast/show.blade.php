{{-- 
<h2>Show Post {{$post->id}}</h2>
<h4>{{$post->title}}</h4>
<p>{{$post->body}}</p> --}}

@extends('layout.master')

@section('navbar')
@include('layout.partial.navbar-data-table')
@endsection

@section('sidebar')
@include('layout.partial.sidebar-data-table')
@endsection


@section('title')
Show
@endsection



@section('content')
<h2>Show Cast {{$cast->id}}</h2>
<h4>{{$cast->nama}}</h4>
<p>{{$cast->umur}}</p>
<p>{{$cast->bio}}</p>
@endsection