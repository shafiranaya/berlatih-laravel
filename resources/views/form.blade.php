<!DOCTYPE html>
<html>
    <head>
        <title>Form</title>
    </head>
    <body>
        <h1>Buat Account Baru!</h1>
        <h3>Sign Up Form</h3>
        <form action="/welcome" method="POST">
            @csrf
            <label>First name:</label><br><br>
            <input type="text" name="first_name"><br><br>
            <label>Last name:</label><br><br>
            <input type="text" name="last_name"><br><br>
            <label>Gender:</label><br><br>
            <input type="radio" name="male" value="Male">Male<br>
            <input type="radio" name="female" value="Female">Female<br>
            <input type="radio" name="other" value="Other">Other<br><br>
            <label>Nationality:</label><br><br>
            <select name="nationality">
                <option value="indonesian">Indonesian</option>
            </select><br><br>
            <label>Language Spoken:</label><br><br>
            <input type="checkbox">Bahasa Indonesia<br>
            <input type="checkbox">English<br>
            <input type="checkbox">Other<br><br>

            <label>Bio:</label><br><br>
            <textarea name="bio"></textarea><br><br>

            <input type="submit" value="Send">
        </form>
    </body>
</html>