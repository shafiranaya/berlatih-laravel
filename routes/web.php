<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('index');
// });

// Route::get('/register', function () {
//     return view('form');
// });

// Route::get('/welcome', function () {
//     return view('welcome');
// });

Route::get('/', 'HomeController@home');
Route::get('/register', 'AuthController@form');
Route::post('/welcome', 'AuthController@send');

// Route::get('/master', function() {
//     return view('layout.master');
// });

Route::get('/table', function() {
    return view('page.table');
});

Route::get('/data-table', function() {
    return view('page.data-table');
});

// Query builder
// Route::get('/cast', 'CastController@index');
// Route::get('/cast/create', 'CastController@create');
// Route::post('/cast', 'CastController@store');
// Route::get('/cast/{cast_id}', 'CastController@show');
// Route::get('/cast/{cast_id}/edit', 'CastController@edit');
// Route::put('/cast/{cast_id}', 'CastController@update');
// Route::delete('/cast/{cast_id}', 'CastController@destroy');

Route::resource('cast','Cast_Controller');
// Route::get('/cast', 'Cast_Controller@index');
// Route::get('/cast/create', 'Cast_Controller@create');
// Route::post('/cast', 'Cast_Controller@store');
// Route::get('/cast/{cast_id}', 'Cast_Controller@show');
// Route::get('/cast/{cast_id}/edit', 'Cast_Controller@edit');
// Route::put('/cast/{cast_id}', 'Cast_Controller@update');
// Route::delete('/cast/{cast_id}', 'Cast_Controller@destroy');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

// Route Middleware
// Route::get('/', function () {
//     //
// })->middleware('web');

// Route::group(['middleware' => ['web']], function () {
//     //
// });

// Route::middleware(['web', 'subscribed'])->group(function () {
//     //
// });